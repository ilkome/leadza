
import $ from 'jquery'
import remodal from 'remodal'

const scrollPhone = () => {
  const $phone = $('.js-phone')
  const windowHeight = $(window).height()
  const offset = windowHeight / 2

  const scrollPosition = $(document).scrollTop()
  const fisrtPosition = $('.inaction').offset().top
  const secondPosition = $('.whatyouget').offset().top

  // scroll phone
  if(scrollPosition > offset && scrollPosition < secondPosition / 1.5) {
    $phone.stop().animate({ top: fisrtPosition })
  } else if (scrollPosition >= secondPosition / 1.5 ) {
    $phone.stop().animate({ top: secondPosition })
  } else {
    $phone.stop().animate({ top: '0px' })
  }
}

$(() => {
  if ($(window).width() > 1200) {
    $(window).scroll(() => scrollPhone())
  }

  $(window).smartResize(() => {
    if ($(window).width() > 1200) {
      $(window).scroll(() => scrollPhone())
    }
  })
})
